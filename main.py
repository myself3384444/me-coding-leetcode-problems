def define_env(env):
    @env.macro
    def code_link(text, path):
        return f'<a href="#" class="code-link" data-code-path="{path}">{text}</a>'
