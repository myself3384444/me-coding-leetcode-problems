'''
Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.
'''

from typing import List

def twoSum(nums: List[int], target: int) -> List[int]:

    ## There are two ways to solve this problem:
    ## 1. Using map
    ## 2. Using two pointers if the array is sorted. 

    target_map = {}


    # X + Y = target, so Y = target - X

    for i in range(len(nums)):

        second_num = target - nums[i]

        if second_num in target_map:
            return [target_map[second_num], i]
        else:
            target_map[nums[i]] = i
        
    return []


print(twoSum(nums = [2,7,11,15], target = 9))