---
title: SQL
tags:
  - Easy
---

|  #  | Title           |  Solution       | Difficulty    | Tag          | 
|-----|---------------- | --------------- | ------------- |--------------|
2007 | [Find Original Array From Doubled Array](https://leetcode.com/problems/find-original-array-from-doubled-array/) | [Python]() | Medium         | variant of [Array of Doubled Pairs](https://leetcode.com/problems/array-of-doubled-pairs/) |