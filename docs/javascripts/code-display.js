function initializeCodeLinks() {
    document.querySelectorAll('.code-link').forEach(link => {
        // Remove existing event listeners to prevent duplicates
        link.removeEventListener('click', handleCodeLinkClick);
        link.addEventListener('click', handleCodeLinkClick);
    });
}

function handleCodeLinkClick(e) {
    e.preventDefault();
    showModal(this.getAttribute('data-code-path'));
}

function showModal(filePath) {
    // Remove any existing modal
    const existingModal = document.querySelector('.code-modal');
    if (existingModal) {
        existingModal.remove();
    }

    fetch(filePath)
        .then(response => response.text())
        .then(code => {
            const modal = document.createElement('div');
            modal.className = 'code-modal';
            modal.innerHTML = `
                <div class="code-modal-content">
                    <div class="code-modal-header">
                        <span class="code-modal-title">${filePath.split('/').pop()}</span>
                        <button class="copy-button">Copy</button>
                        <span class="close">&times;</span>
                    </div>
                    <pre><code class="language-python">${escapeHtml(code)}</code></pre>
                </div>
            `;
            document.body.appendChild(modal);
            
            const codeElement = modal.querySelector('code');
            hljs.highlightElement(codeElement);
            addLineNumbers(codeElement);
            
            modal.querySelector('.close').onclick = function() {
                modal.remove();
            };
            
            modal.querySelector('.copy-button').onclick = function() {
                navigator.clipboard.writeText(code).then(() => {
                    this.textContent = 'Copied!';
                    setTimeout(() => this.textContent = 'Copy', 2000);
                });
            };
            
            modal.addEventListener('click', function(event) {
                if (event.target === modal) {
                    modal.remove();
                }
            });
        });
}

function addLineNumbers(codeElement) {
    const lines = codeElement.innerHTML.split('\n');
    codeElement.innerHTML = lines.map((line, index) => 
        `<span class="line-number">${index + 1}</span>${line}`
    ).join('\n');
}

function escapeHtml(unsafe) {
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}

// Initialize on DOMContentLoaded
document.addEventListener('DOMContentLoaded', initializeCodeLinks);

// Re-initialize on each page load for single-page applications
document.addEventListener('DOMContentLoaded', function() {
    if (window.MutationObserver) {
        const observer = new MutationObserver(function(mutations) {
            if (mutations.some(mutation => mutation.addedNodes.length > 0)) {
                initializeCodeLinks();
            }
        });
        observer.observe(document.body, { childList: true, subtree: true });
    }
});
